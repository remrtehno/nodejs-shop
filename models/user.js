const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  cart: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true,
        },
        quantity: {type: Number, required: true},
      },
    ],
  },
})

userSchema.methods.addToCart = function (product) {
  const productIndex = this.cart.items.findIndex(
    ({productId}) => productId.toString() === product,
  )

  if (productIndex < 0) {
    this.cart.items.push({
      productId: product,
      quantity: 1,
    })
  } else {
    const productInCart = this.cart.items[productIndex]

    this.cart.items[productIndex] = {
      ...productInCart,
      productId: productInCart.productId,
      quantity: productInCart.quantity + 1,
    }
  }

  this.save()
}

module.exports = mongoose.model('User', userSchema)

// const fs = require('fs')

// class User {
//   constructor(name) {
//     this.name = name
//   }

//   static allUsers() {
//     return new Promise((resolve, reject) => {
//       fs.readFile('./mock/users.txt', 'utf8', (err, data) => {
//         resolve(data)
//       })
//     })
//   }

//   save() {
//     return new Promise((resolve, reject) => {
//       fs.appendFile('./mock/users.txt', '\n' + this.name, () => {
//         resolve()
//       })
//     })
//   }
// }

// module.exports = User
