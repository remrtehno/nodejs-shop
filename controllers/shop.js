const product = require('../models/product')
const user = require('../models/user')

exports.allProducts = (req, res, next) => {
  product
    .find()
    .then(products => {
      res.render('shop', {title: 'Shop', products})
    })
    .catch(err => console.error(err))
}

exports.addToCart = (req, res, next) => {
  user
    .findOne()
    .then(result => {
      if (!result) {
        return res.redirect('/')
      }

      result.addToCart(req.params.productId)
      res.redirect('/')
    })
    .catch(err => console.log(err))
}
