const User = require('../models/user')
const bcrypt = require('bcrypt')

exports.login = (req, res, next) => {
  res.render('auth/login')
}

exports.register = (req, res, next) => {
  res.render('auth/register')
}

exports.logout = (req, res, next) => {
  req.session.destroy(err => {
    console.error(err)

    res.redirect('/')
  })
}

exports.postLogin = (req, res, next) => {
  const {email, password} = req.body

  User.findOne({email})
    .then(user => {
      if (!user) {
        req.flash('errors', 'Ivalid mail or password.')
        return res.redirect('/login')
      }
      return bcrypt
        .compare(password, user.password)
        .then(isMatched => {
          if (isMatched) {
            req.session.isAuth = true
            req.session.user = user

            return req.session.save(err => {
              console.error(err)
              res.redirect('/')
            })
          }

          req.flash('errors', 'Ivalid mail or password.')
          res.redirect('/login')
        })
        .catch(err => {
          console.log(err)
          res.redirect('/login')
        })
    })
    .catch(err => console.error(err))
}

exports.postRegister = (req, res, next) => {
  const {email, password, passwordConfirm} = req.body

  if (passwordConfirm !== password) {
    req.flash('errors', 'Password does not match with confirmation password.')
    res.redirect('/register')
  }

  User.findOne({
    email,
  })
    .then(user => {
      if (user) {
        req.flash('errors', 'You have used an existed E-mail, try to use another one.')
        return res.redirect('/register')
      }

      return bcrypt
        .hash(password, 12)
        .then(hashedPassword => {
          const newUser = new User({
            name: email,
            email,
            password: hashedPassword,
          })

          return newUser.save()
        })
        .then(result => {
          res.redirect('/login')
        })
    })
    .catch(err => console.error(err))
}
