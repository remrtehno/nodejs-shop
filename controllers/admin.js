const bcrypt = require('bcrypt')

const User = require('../models/user')
const Product = require('../models/product')

exports.users = (req, res, next) => {
  User.find()
    .then(data => {
      res.setHeader('Content-Type', 'text/html')
      res.render('create-users', {users: data})
    })
    .catch(err => console.error(err))
}

exports.editUser = (req, res, next) => {
  const usersProducts = Product.find({user: req.params.userId}).populate('user')

  const users = User.findById(req.params.userId).populate(
    'cart.items.productId',
  )

  Promise.all([usersProducts, users])
    .then(([products, user]) => {
      res.setHeader('Content-Type', 'text/html')

      res.render('edit-user', {user, products})
    })
    .catch(err => console.error(err))
}

exports.updateUser = (req, res, next) => {
  User.findById(req.body.userId)
    .then(data => {
      data.name = req.body.name
      data.save()
    })
    .then(() => {
      res.redirect('/admin/users')
    })
    .catch(err => console.error(err))
}

exports.deleteUser = (req, res, next) => {
  User.findByIdAndRemove(req.body.userId)
    .then(() => {
      res.redirect('/admin/users')
    })
    .catch(err => console.error(err))
}

exports.addProductToUser = (req, res, next) => {
  const {title, price, userId} = req.body

  const product = new Product({
    title,
    price,
    user: userId,
  })

  product.save().then(() => {
    res.redirect('/admin/edit-user/' + userId)
  })
}

exports.createUser = (req, res, next) => {
  bcrypt
    .hash(req.body.password, 12)
    .then(hashedPassword => {
      const newUser = new User({
        name: req.body.username,
        email: 'test@mail.com',
        password: hashedPassword,
      })

      newUser.save().then(data => {
        res.redirect('/admin/users')
      })
    })

    .catch(err => console.error(err))
}
