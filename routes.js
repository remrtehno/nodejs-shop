const fs = require('fs')
const htmlTemplate = require('./views/layout')
const createUserForm = require('./views/create-user')

const routes = (req, res) => {
  const {url, method} = req

  if (url === '/') {
    res.setHeader('Content-Type', 'text/html')
    res.write(htmlTemplate('<h1>Hello from my Node.js Server!</h1>'))
    res.end()
  }

  if (url === '/users') {
    fs.readFile('./mock/users.txt', 'utf8', function (err, data) {
      res.setHeader('Content-Type', 'text/html')

      const users = data.split(/\n/).reduce((acc, user) => {
        return acc + `<li>${user}</li>`
      }, '')

      res.write(htmlTemplate(`<ul>${users}</ul> ${createUserForm()}`))

      res.end()
    })
  }

  if (url == '/create-user' && method == 'POST') {
    const data = []
    req.on('data', chunk => {
      data.push(chunk)
    })

    return req.on('end', () => {
      const postData = Buffer.concat(data).toString()
      const newUser = postData.split('=')[1]

      fs.appendFile('./mock/users.txt', '\n' + newUser, () => {
        res.statusCode = 302
        res.setHeader('Location', '/users')

        return res.end()
      })
    })
  }
}

module.exports = routes
