const express = require('express')

const adminController = require('../controllers/admin')

const router = express.Router()

// app.use('/users', (req, res, next)  - possible to use "use" method
router.get('/users', adminController.users)
router.get('/edit-user/:userId', adminController.editUser)
router.post('/update-user', adminController.updateUser)
router.post('/delete-user', adminController.deleteUser)

router.post('/add-product', adminController.addProductToUser)

router.post('/create-user', adminController.createUser)

module.exports = router
