const express = require('express')

const authController = require('../controllers/auth')

const router = express.Router()

router.get('/login', authController.login)
router.get('/register', authController.register)
router.get('/logout', authController.logout)

router.post('/login', authController.postLogin)
router.post('/register', authController.postRegister)

module.exports = router
