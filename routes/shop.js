const express = require('express')

const shopController = require('../controllers/shop')
const isAuth = require('../middleware/is-auth')

const router = express.Router()

router.get('/', shopController.allProducts)

router.get('/add-to-cart/:productId', isAuth, shopController.addToCart)

module.exports = router
