const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const MongoDBStore = require('connect-mongodb-session')(session)
const csrf = require('csurf')
const flash = require('connect-flash')

const rootDir = require('./util/path')

const adminRoutes = require('./routes/admin')
const shopRoutes = require('./routes/shop')
const authRoutes = require('./routes/auth')
const isAuth = require('./middleware/is-auth')
const errors = require('./middleware/errors')

const store = new MongoDBStore({
  uri: 'mongodb+srv://test:O7J3xPxKMgW0BBO0@cluster0.nytno.mongodb.net/?retryWrites=true&w=majority',
  collection: 'sessions',
})

const app = express()

const csrfProtection = csrf()

//pre config
app.use(express.static(path.join(rootDir, 'public')))
app.use(bodyParser.urlencoded({extended: false}))
app.set('view engine', 'ejs')
app.use(
  session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
    store,
  }),
)
app.use(csrfProtection)
app.use(flash())
app.use(errors)

app.use((req, res, next) => {
  console.log('The Middleware 1')
  res.extraData = 'Extra Data itself in res'
  req.extraData = 'Extra Data itself in req'
  next()
})

app.use((req, res, next) => {
  console.log('The Middleware 2')
  console.log('Extra data from req', req.extraData)
  console.log('Extra data from res', res.extraData)
  next()
})

app.use((req, res, next) => {
  res.locals.isAuth = req.session.isAuth
  res.locals.csrfToken = req.csrfToken()

  next()
})

//with prefix "admin"
app.use('/admin', isAuth, adminRoutes)
app.use(shopRoutes)
app.use(authRoutes)
// handle 404
app.use((req, res, next) => {
  res.status(404).setHeader('set-cookie', 'value').render('404')
})

mongoose
  .connect(
    'mongodb+srv://test:O7J3xPxKMgW0BBO0@cluster0.nytno.mongodb.net/?retryWrites=true&w=majority',
  )
  .then(() => {
    app.listen(3001)
    console.log(1);
  })
  .catch(err => {
    throw err
  })
